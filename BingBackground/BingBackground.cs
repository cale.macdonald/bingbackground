﻿namespace BingBackground
{
    using Nito.AsyncEx;

    public static class BingBackground
    {
        public static void Main()
        {
            AsyncContext.Run(() => MainAsync());
        }

        private static async void MainAsync()
        {
            string url = await RemoteApi.GetCurrentImageUrl();
            BackgroundImage image = BackgroundImage.FromUrl(url);
            image.SetBackground(GetPosition());
        }

        private static PicturePosition GetPosition()
        {
            PicturePosition position;

            switch (Properties.Settings.Default.Position)
            {
                case "Tile":
                    position = PicturePosition.Tile;
                    break;
                case "Center":
                    position = PicturePosition.Center;
                    break;
                case "Stretch":
                    position = PicturePosition.Stretch;
                    break;
                case "Fit":
                    position = PicturePosition.Fit;
                    break;
                case "Fill":
                    position = PicturePosition.Fill;
                    break;
                default:
                    position = PicturePosition.Fit;
                    break;
            }

            return position;
        }
    }
}
