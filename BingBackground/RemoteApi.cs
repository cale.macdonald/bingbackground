﻿namespace BingBackground
{
    using System;
    using System.Drawing;
    using System.Net;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using Newtonsoft.Json;

    public static class RemoteApi
    {
        public static async Task<string> GetCurrentImageUrl()
        {
            string urlBase = await GetBackgroundUrlBase();
            return urlBase + GetResolutionExtension(urlBase);
        }

        private static async Task<string> GetBackgroundUrlBase()
        {
            dynamic jsonObject = await DownloadJson();
            return "https://www.bing.com" + jsonObject.images[0].urlbase;
        }

        private static async Task<dynamic> DownloadJson()
        {
            using (var webClient = new WebClient())
            {
                string jsonString = await webClient.DownloadStringTaskAsync(new Uri("https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US"));
                return JsonConvert.DeserializeObject<dynamic>(jsonString);
            }
        }

        private static string GetResolutionExtension(string url)
        {
            Rectangle resolution = Screen.PrimaryScreen.Bounds;
            string widthByHeight = resolution.Width + "x" + resolution.Height;
            string potentialExtension = "_" + widthByHeight + ".jpg";

            return WebsiteExists(url + potentialExtension) ? potentialExtension : "_1920x1080.jpg";
        }

        private static bool WebsiteExists(string url)
        {
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "HEAD";
                var response = (HttpWebResponse)request.GetResponse();
                return response.StatusCode == HttpStatusCode.OK;
            }
            catch
            {
                return false;
            }
        }

    }
}