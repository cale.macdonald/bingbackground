﻿namespace BingBackground
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Runtime.InteropServices;

    using Microsoft.Win32;

    public class BackgroundImage
    {
        private readonly Image image;

        private BackgroundImage(Image image)
        {
            this.image = image;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

        public void SetBackground(PicturePosition position)
        {
            string directory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "Bing Backgrounds", DateTime.Now.Year.ToString());
            Directory.CreateDirectory(directory);
            string imagePath = Path.Combine(directory, DateTime.Now.ToString("M-d-yyyy") + ".bmp");

            this.image.Save(imagePath);

            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(Path.Combine("Control Panel", "Desktop"), true))
            {
                switch (position)
                {
                    case PicturePosition.Tile:
                        key.SetValue("PicturePosition", "0");
                        key.SetValue("TileWallpaper", "1");
                        break;
                    case PicturePosition.Center:
                        key.SetValue("PicturePosition", "0");
                        key.SetValue("TileWallpaper", "0");
                        break;
                    case PicturePosition.Stretch:
                        key.SetValue("PicturePosition", "2");
                        key.SetValue("TileWallpaper", "0");
                        break;
                    case PicturePosition.Fit:
                        key.SetValue("PicturePosition", "6");
                        key.SetValue("TileWallpaper", "0");
                        break;
                    case PicturePosition.Fill:
                        key.SetValue("PicturePosition", "10");
                        key.SetValue("TileWallpaper", "0");
                        break;
                }
            }

            const int SetDesktopBackground = 20;
            const int UpdateIniFile = 1;
            const int SendWindowsIniChange = 2;

            SystemParametersInfo(SetDesktopBackground, 0, imagePath, UpdateIniFile | SendWindowsIniChange);
        }

        public static BackgroundImage FromUrl(string url)
        {
            WebRequest request = WebRequest.Create(url);
            WebResponse reponse = request.GetResponse();
            Stream stream = reponse.GetResponseStream();
            return new BackgroundImage(Image.FromStream(stream));
        }
    }
}